.data

input:	.asciiz "test1.txt"
output:	.asciiz "transposed.pgm"	#used as output
buffer:  .space 2048		# buffer for upto 2048 bytes
newbuff: .space 2048

#any extra data you specify MUST be after this line 
array: 	.space 672
error1:	.asciiz "File not valid"
text1:  .asciiz "P2\n7 24\n15"

	.text
	.globl main

main:	la $a0,input 		#readfile takes $a0 as input
	jal readfile


	la $a0,buffer		#$a0 will specify the "2D array" we will be flipping
	la $a1,newbuff		#$a1 will specify the buffer that will hold the flipped array.
    	jal transpose


	la $a0, output		#writefile will take $a0 as file location
	la $a1,newbuff		#$a1 takes location of what we wish to write.
	jal writefile

	li $v0,10		# exit
	syscall

readfile:
#done in Q1
	li $v0,13		# Open the file to be read,using $a0
	li $a1, 0		# Open in read-only
	li $a2, 0		# mode is ignored
	syscall
	
	move $s0, $v0        	# use correct file descriptor, and point to buffer	
	blt $s0,0,error_1 	# Conduct error check, to see if file exists

	li $v0, 14           	# read from file
	move $a0, $s0		# load file descriptor
	la $a1, buffer        	# load saved bytes
	li $a2, 2048         	# hardcode maximum number of chars to read
	syscall 
	
	# the text of the string is now in buffer
	li   $v0, 16       	# close the file (make sure to check for errors)
  	move $a0, $s0
  	syscall
  	
  	la $v1, buffer 		# address of the ascii string you just read is returned in $v1.
  	
	jr $ra			# go back to main routine

error_1:
	li $v0,4		# Print error message
	la $a0, error1
	syscall
	li $v0,10		# exit
	syscall

transpose:
 #Create array using same steps as flipper 
 #Only difference is that the array is in order this time
	li $t0, -4
	move $t2, $a0
	j iterate

iterate:
	
	lb $t3, ($t2)
	beq $t3, 0, print
	bge $t3, 48, number
	add $t2, $t2, 1
	j iterate

number:
	add $t3, $t3, -48
	add $t2, $t2, 1
	lb $t4, ($t2)
	bge $t4, 48, double
	addiu $t0, $t0, 4
	sb $t3, array($t0)
	add $t2, $t2, 1
	j iterate
	double:
	add $t2, $t2, 1
	add $t4, $t4, -48
	mul $t5, $t3, 10
	add $t3, $t5, $t4
	addiu $t0, $t0, 4
	sb $t3, array($t0)
	add $t2, $t2, 1
	j iterate

print: 	#once array is created
	move $t6, $a1
	li $t0, 25
	li $t1, 7
	li $t2, 668 #96 and 576 will be subtracted to this number (giving first element of array)
yax:
	beq $t0, 0, ex #Similar to yax in flipper...
	beq $t1, 7, ad
	lb $a0, array($t2)
	blt $a0, 10, secsp
	add $a0, $a0, -10
	add $a0, $a0, 48
	li $a1, 49
	sb $a1, ($t6)
	add $t6, $t6, 1
	sb $a0, ($t6)
	add $t6, $t6, 1
	addiu $t2, $t2, 96 #Except we go through the array by steps of 24 integers
	#This way, we go through each column at a time as opposed to each row
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	secsp:
	add $a0, $a0, 48
	sb $a0, ($t6)
	add $t6, $t6, 1
	addiu $t2, $t2, 96
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, 1
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	ad: #Once the last colunm is reached...
	addiu $t2, $t2, -96 #we go back to first column of same row
	addiu $t2, $t2, -576
	addiu $t2, $t2, 4 # and add 4 bytes thus going to the next row
	add $t0, $t0, -1
	add $t1, $0, $0
	li, $a0, 10
	sb $a0, ($t6)
	add $t6, $t6, 1
	beq $t1, 24, ad
	lb $a0, array($t2)
	blt $a0, 10, secsp
	add $a0, $a0, -10
	add $a0, $a0, 48
	li $a1, 49
	sb $a1, ($t6)
	add $t6, $t6, 1
	sb $a0, ($t6)
	add $t6, $t6, 1
	addiu $t2, $t2, 96
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	ex: #when the last row is done, go back to main
	jr $ra
	
	
#Can assume 24 by 7 again for the input.txt file
#Try to understand the math before coding!
#Can assume 24 by 7 again for the input.txt file
#Try to understand the math before coding!

writefile:	
	move $t0, $a1
	li   $v0, 13       	# open file to be written to, using $a0.
	li   $a1, 1        	# Open for writing
	li   $a2, 0        	# mode is ignored
	syscall            	# open a file 
	move $s1, $v0      	# save the file descriptor 

	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	la   $a1, text1    	# write the specified characters as seen on assignment PDF
	li   $a2, 10     	# hardcoded buffer length
	syscall
	
	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	move $a1, $t0    	# write the specified characters as seen on assignment PDF
	li   $a2, 528		# hardcoded buffer length
	syscall

	li   $v0, 16       	# close the file (make sure to check for errors)
	move $a0, $s1
	syscall
	jr $ra			# go back to main routine

#slightly different from Q1.
#use as many arguments as you would like to get this to work.
#make sure the header matches the new dimensions!
