.data

input:	.asciiz "test1.txt"
output:	.asciiz "cropped.pgm"	#used as output
buffer:  .space 2048		# buffer for upto 2048 bytes
newbuff: .space 2048
x1: .word 3
x2: .word 9
y1: .word 2
y2: .word 7
headerbuff: .space 2048  #stores header
#any extra .data you specify MUST be after this line 
array: 	.space 672
error1:	.asciiz "File not valid"
text1:  .asciiz "P2\n"
text2:  .asciiz "\n15"

	.text
	.globl main

main:	la $a0,input		#readfile takes $a0 as input
	jal readfile


    #load the appropriate values into the appropriate registers/stack positions
    #appropriate stack positions outlined in function*
	#a0=x1
	#a1=x2
	#a2=y1
	#a3=y2
	#16($sp)=buffer
	#20($sp)=newbuffer that will be made
	la $a0, x1
	la $a1, x2
	la $a2, y1
	la $a3, y2
	#Store all values to the stack...
	#$a0 is in first position (0th), $a1 is in second (4th)
	addiu $sp, $sp, -4
	la $t0, newbuff
	sw $t0, 0($sp)
	addiu $sp, $sp, -4
	la $t0 buffer
	sw $t0, 0($sp)
	addiu $sp, $sp, -4
	lb $a3, ($a3)
	sw $a3, 0($sp)
	addiu $sp, $sp, -4
	lb $a2, ($a2)
	sw $a2, 0($sp)
	addiu $sp, $sp, -4
	lb $a1, ($a1)
	sw $a1, 0($sp)
	addiu $sp, $sp, -4
	lb $a0, ($a0)
	sw $a0, 0($sp)
	#As requested buffer is in 16th and newbuff is in 20th
	
	jal crop
	la $a0, output		#writefile will take $a0 as file location
	la $a1,newbuff		#$a1 takes location of what we wish to write.
	#add what ever else you may need to make this work.
	jal writefile
	addiu $sp, $sp, 24
	li $v0,10		# exit
	syscall

readfile:
	li $v0,13		# Open the file to be read,using $a0
	li $a1, 0		# Open in read-only
	li $a2, 0		# mode is ignored
	syscall
	
	move $s0, $v0        	# use correct file descriptor, and point to buffer	
	blt $s0,0,error_1 	# Conduct error check, to see if file exists

	li $v0, 14           	# read from file
	move $a0, $s0		# load file descriptor
	la $a1, buffer        	# load saved bytes
	li $a2, 2048         	# hardcode maximum number of chars to read
	syscall 
	
	# the text of the string is now in buffer
	li   $v0, 16       	# close the file (make sure to check for errors)
  	move $a0, $s0
  	syscall
  	
  	la $v1, buffer 		# address of the ascii string you just read is returned in $v1.
  	
	jr $ra			# go back to main routine

error_1:
	li $v0,4		# Print error message
	la $a0, error1
	syscall
	li $v0,10		# exit
	syscall


crop: #create array as in transpose
	li $t0, -4
	lw $t2, 16($sp)
	j iterate

iterate:
	
	lb $t3, ($t2)
	beq $t3, 0, print
	bge $t3, 48, number
	add $t2, $t2, 1
	j iterate

number:
	add $t3, $t3, -48
	add $t2, $t2, 1
	lb $t4, ($t2)
	bge $t4, 48, double
	addiu $t0, $t0, 4
	sb $t3, array($t0)
	add $t2, $t2, 1
	j iterate
	double:
	add $t2, $t2, 1
	add $t4, $t4, -48
	mul $t5, $t3, 10
	add $t3, $t5, $t4
	addiu $t0, $t0, 4
	sb $t3, array($t0)
	add $t2, $t2, 1
	j iterate

print: 	#once array is complete
	lw $t6, 20($sp) # load newbuff from stack
	lw $t1, 4($sp) #load all bounds
	lw $t0, 8($sp)
	li $t2, 0
	lw $t3, 4($sp)
	lw $t4, 12($sp)
	lw $s5, 0($sp)
	li $s0, 24
	subu $s0, $s0, $t3
	add $s0, $s0, $s5
	mul $s0, $s0, 4#Set value by which we increase position once end of line is reached
	mul $s1, $t0, 24
	add $s1, $s1, $s5
	mul $s1, $s1, 4 
	add $t2 $0, $s1 #Set starting point
	sub $t2, $t2, $s0 
yax:
	beq $t1, $t3, ad #Iterate through in a fashion similar as previous numbers
	lb $a0, array($t2)
	blt $a0, 10, secsp
	add $a0, $a0, -10 #Only bounds are different...
	add $a0, $a0, 48 # And we go through the list in order
	li $a1, 49
	sb $a1, ($t6)
	add $t6, $t6, 1
	sb $a0, ($t6)
	add $t6, $t6, 1
	addiu $t2, $t2, 4
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	secsp:
	add $a0, $a0, 48
	sb $a0, ($t6)
	add $t6, $t6, 1
	addiu $t2, $t2, 4
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, 1
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	ad:
	bge $t0, $t4, ex
	add $t0, $t0, 1
	lw $t1, 0($sp)
	li, $a0, 10
	sb $a0, ($t6)
	add $t6, $t6, 1
	beq $t1, 24, ad
	add $t2, $t2, $s0 #When new line is reached, we add appropriate spacing and increase array position by number defined above
	lb $a0, array($t2)
	blt $a0, 10, secsp
	add $a0, $a0, -10
	add $a0, $a0, 48
	li $a1, 49
	sb $a1, ($t6)
	add $t6, $t6, 1
	sb $a0, ($t6)
	add $t6, $t6, 1
	addiu $t2, $t2, 4
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	ex:
	jr $ra

#Remember to store ALL variables to the stack as you normally would,
#before starting the routine.
#Try to understand the math before coding!
#There are more than 4 arguments, so use the stack accordingly.

writefile:	
	move $t6, $a1
	li   $v0, 13       	# open file to be written to, using $a0.
	li   $a1, 1        	# Open for writing
	li   $a2, 0        	# mode is ignored
	syscall            	# open a file 
	move $s1, $v0      	# save the file descriptor 

	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	la   $a1, text1    	# write the specified characters as seen on assignment PDF
	li   $a2, 3     	# hardcoded buffer length
	syscall
	
	lb $t0, 0($sp) #Header must be changed accordingly
	lb $t1, 4($sp) #load x bounds
	la $t2, headerbuff
	li $t5, 4
	subu $t0, $t1 $t0
	add $t7, $0, $t0 #find width that is included
	bge $t0, 20, twenty # if width is ge 20
	bge $t0, 10, ten # if width ge 10
	add $t0, $t0, 48 #if width less than 10, convert it to aascii
	sb $t0, ($t2)
	add $t5, $t5, -1 #and store it in new buffer
	j second #for greater than 20, include a 2 before it 
	ten: 
		li $t1, 49
		sb $t1, ($t2)
		add $t2, $t2, 1
		add $t0, $t0, -10
		add $t0, $t0, 48
		sb $t0, ($t2)
		j second
	twenty: # for greater than 10 include a 1 before it
		li $t1, 50
		sb $t1, ($t2)
		add $t2, $t2, 1
		add $t0, $t0, -20
		add $t0, $t0, 48
		sb $t0, ($t2)
		j second
	second:
		add $t2, $t2, 1
		li $t0, 32 # next print a space
		sb $t0, ($t2)
		lb $t4, 8($sp)
		lb $t3, 12($sp)
		subu $t4, $t3 $t4
		mul $t7, $t7, $t4
		add $a0, $0, $t7
		mul $t7, $t7, 3
		add $t7, $t7, $t4 #and store height. Since it is at most 7, me need not check for double digits
		add $t4, $t4, 48
		add $t2, $t2, 1
		sb $t4, ($t2)
		###NOTE: Cropper crops by array convention. That is, it includes the lower bound and excludes the upper bound
		# This means setting x1 to 2 and x2 to 5 will give columns 2;3;4. Same for y
		
	li   $v0, 15       	# system call for write to file
	move $a0, $s1    	# file descriptor 
	la $a1, headerbuff    	# write the specified characters as seen on assignment PDF
	move $a2, $t5		# hardcoded buffer length
	syscall
	
	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	la   $a1, text2    	# write the specified characters as seen on assignment PDF
	li   $a2, 3     	# hardcoded buffer length
	syscall
	

	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	move $a1, $t6    	# write the specified characters as seen on assignment PDF
	add $a2, $0, $t7     	# hardcoded buffer length
	syscall

	li   $v0, 16       	# close the file (make sure to check for errors)
	move $a0, $s1
	syscall
	jr $ra			# go back to main routine
