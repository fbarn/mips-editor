# MIPS Editor

A simple pgm editor written in MIPS assembly.

Supports adding a border, flipping, cropping, and transposing.