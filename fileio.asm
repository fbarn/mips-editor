
.data

input:	.asciiz "test1.txt" #used as input
output:	.asciiz "copy.pgm"	#used as output
error1:	.asciiz "File not valid."
text1:  .asciiz "P2\n24 7\n15\n"
buffer: .space 2048		# buffer for upto 2048 bytes

	.text
	.globl main

main:	
	la $a0,input		# readfile takes $a0 as input
	jal readfile

	la $a0, output		# writefile will take $a0 as file location
	la $a1,buffer		# $a1 takes location of what we wish to write.
	jal writefile

	li $v0,10		# exit
	syscall

readfile:
	li $v0,13		# Open the file to be read,using $a0
	li $a1, 0		# Open in read-only
	li $a2, 0		# mode is ignored
	syscall
	
	move $s0, $v0        	# use correct file descriptor, and point to buffer	
	blt $s0,0,error_1 	# Conduct error check, to see if file exists

	li $v0, 14           	# read from file
	move $a0, $s0		# load file descriptor
	la $a1, buffer        	# allocate space for the bytes loaded
	li $a2, 2048         	# hardcode maximum number of chars to read
	syscall 
	
	# the text of the string is now in buffer
	
	la $a0, buffer        	# load address of string to be printed
	li $v0, 4            	# print string
	syscall
	
	li   $v0, 16       	# close the file
  	move $a0, $s0
  	syscall
  	
  	la $v1, buffer 		# address of the ascii string you just read is returned in $v1.
  	
	jr $ra			# go back to main routine

error_1:
	li $v0,4		# Print error message
	la $a0, error1
	syscall
	li $v0,10		# exit
	syscall

writefile:
	move $t0, $a1
	li   $v0, 13       	# open file to be written to, using $a0.
	li   $a1, 1        	# Open for writing
	li   $a2, 0        	# mode is ignored
	syscall            	# open a file 
	move $s1, $v0      	# save the file descriptor 

	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	la   $a1, text1    	# write the specified characters as seen on assignment PDF
	li   $a2, 11     	# hardcoded buffer length
	syscall
	
	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	move $a1, $t0    	# write the specified characters as seen on assignment PDF
	li   $a2, 496    	# hardcoded buffer length
	syscall

	li   $v0, 16       	# close the file (make sure to check for errors)
	move $a0, $s1
	syscall
	jr $ra			# go back to main routine



