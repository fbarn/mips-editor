.data

input:	.asciiz "test1.txt"
output:	.asciiz "flipped.pgm"	#used as output
axis: 	.word 0 # 0=flip around x-axis....1=flip around y-axis
buffer:  .space 2048		# buffer for upto 2048 bytes
newbuff: .space 2048

#any extra data you specify MUST be after this line 
array: 	.space 672
error1:	.asciiz "File not valid"
text1:  .asciiz "P2\n24 7\n15"

	.text
	.globl main

main:
    la $a0,input	#readfile takes $a0 as input
    jal readfile


	la $a0,buffer		#$a0 will specify the "2D array" we will be flipping
	la $a1,newbuff		#$a1 will specify the buffer that will hold the flipped array.
	la $a2,axis        #either 0 or 1, specifying x or y axis flip accordingly
	jal flip


	la $a0, output		#writefile will take $a0 as file location we wish to write to.
	la $a1,newbuff		#$a1 takes location of what data we wish to write.
	jal writefile

	li $v0,10		# exit
	syscall

readfile:
#done in Q1
li $v0,13		# Open the file to be read,using $a0
	li $a1, 0		# Open in read-only
	li $a2, 0		# mode is ignored
	syscall
	
	move $s0, $v0        	# use correct file descriptor, and point to buffer	
	blt $s0,0,error_1 	# Conduct error check, to see if file exists

	li $v0, 14           	# read from file
	move $a0, $s0		# load file descriptor
	la $a1, buffer        	# load saved bytes
	li $a2, 2048         	# hardcode maximum number of chars to read
	syscall 
	
	# the text of the string is now in buffer
	li   $v0, 16       	# close the file (make sure to check for errors)
  	move $a0, $s0
  	syscall
  	
  	la $v1, buffer 		# address of the ascii string you just read is returned in $v1.
  	
	jr $ra			# go back to main routine

error_1:
	li $v0,4		# Print error message
	la $a0, error1
	syscall
	li $v0,10		# exit
	syscall

flip:
	move $t2, $a0 		#load buffer address
	li $t0, 672		#get starting point of array (bits will get put backwards, so this is end)
	j iterate		#Place appropriate bits in buffer one by one

iterate:
	lb $t3, ($t2)
	beq $t3, 0, print	#file is over
	bge $t3, 48, number #If it is a number, add it, otherwise don't
	add $t2, $t2, 1		#move to next position
	j iterate

number:
	add $t3, $t3, -48	# convert number from aascii
	add $t2, $t2, 1		#move to next position
	lb $t4, ($t2)		
	bge $t4, 48, double	#go to "double" if double digits (that is, next position is part of original number)
	addiu $t0, $t0, -4	#otherwise, store the integer as is and move on
	sb $t3, array($t0)	
	add $t2, $t2, 1
	j iterate
	double:		#if double digits, take the first bit, multiply it by 10 and add it to second bit
	add $t2, $t2, 1	
	add $t4, $t4, -48
	mul $t5, $t3, 10	
	add $t3, $t5, $t4
	addiu $t0, $t0, -4 #store that bit in array
	sb $t3, array($t0)
	add $t2, $t2, 1
	j iterate

print: 	lb $t0, ($a2)
	beq $t0, 1, yax1
	beq $t0, 0, xax1 #read user's input to see on which axis to flip
	
	
#Can assume 24 by 7 again for the input.txt file
#Try to understand the math before coding!
	

yax1: #If y axis is chosen
	move $t6, $a1 #load the new buffer
	li, $a0, 10 # we start by adding a newline su that the file does not interfere with header
	sb $a0, ($t6) 
	add $t6, $t6, 584 #we go to the beginnign of the last line
	li $t0, 8
	li $t1, 24
	li $t2, 0 #set bounds for array... 0-24 for x; 0-7 for y
yax:
	beq $t0, 0, ex
	beq $t1, 24, ad #we go through every element of the array
	lb $a0, array($t2) #we read the rows in reverse order and the elements of that row in order
	blt $a0, 10, secsp #If number is less than 10
	add $a0, $a0, -10 #if double digits
	add $a0, $a0, 48
	li $a1, 49	#subtract 10, save this value
	sb $a1, ($t6)	#since it cannot be greater than 10, set 1 as its first digit and saved value as its second
	add $t6, $t6, 1 # convert both these digits to aascii and store them consecutively
	sb $a0, ($t6)
	add $t6, $t6, 1
	addiu $t2, $t2, 4
	li, $a0, 32		#add only one space
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	secsp: #if single digits
	add $a0, $a0, 48 #convert back to aascii
	sb $a0, ($t6) #store that one digit
	add $t6, $t6, 1
	addiu $t2, $t2, 4
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, 1 # and two spaces
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	ad: #if end of line is reached
	add $t0, $t0, -1 #increase y position by 1, set x position to 0
	add $t1, $0, $0
	li, $a0, 10 #add newline character
	sb $a0, ($t6)
	add $t6, $t6, -145
	beq $t1, 24, ad
	lb $a0, array($t2) #add the first number character using same steps as above
	blt $a0, 10, secsp
	add $a0, $a0, -10
	add $a0, $a0, 48
	li $a1, 49
	sb $a1, ($t6)
	add $t6, $t6, 1
	sb $a0, ($t6)
	add $t6, $t6, 1
	addiu $t2, $t2, 4
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	ex: # if end of array is reached, go back to main
	jr $ra

xax1:	move $t6, $a1 #If x axis is chosen,
	add $t6, $t6, -73
	li $t0, 8
	li $t1, 24
	li $t2, 0 #Similar logic as above
	# However, we read the rows in order, and the elements of each row in reverse order

xax:
	beq $t0, 0, xex
	beq $t1, 24, xad
	lb $a0, array($t2)
	blt $a0, 10, xsecsp
	add $a0, $a0, -10
	add $a0, $a0, 48
	li $a1, 49
	sb $a0, ($t6)
	add $t6, $t6, -1
	sb $a1, ($t6)
	add $t6, $t6, -1
	addiu $t2, $t2, 4
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, -1
	add $t1, $t1, 1
	j xax
	xsecsp:
	add $a0, $a0, 48
	sb $a0, ($t6)
	add $t6, $t6, -1
	addiu $t2, $t2, 4
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, -1
	sb $a0, ($t6)
	add $t6, $t6, -1
	add $t1, $t1, 1
	j xax
	xad:
	add $t0, $t0, -1
	add $t1, $0, $0
	li, $a0, 10
	sb $a0, ($t6)
	add $t6, $t6, +145
	lb $a0, array($t2)
	blt $a0, 10, xsecsp
	add $a0, $a0, -10
	add $a0, $a0, 48
	li $a1, 49
	sb $a0, ($t6)
	add $t6, $t6, -1
	sb $a1, ($t6)
	add $t6, $t6, -1
	addiu $t2, $t2, 4
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, -1
	add $t1, $t1, 1
	j xax
	xex:
	jr $ra

writefile:
	move $t0, $a1
	li   $v0, 13       	# open file to be written to, using $a0.
	li   $a1, 1        	# Open for writing
	li   $a2, 0        	# mode is ignored
	syscall            	# open a file 
	move $s1, $v0      	# save the file descriptor 

	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	la   $a1, text1    	# write the specified characters as seen on assignment PDF
	li   $a2, 10     	# hardcoded buffer length
	syscall
	
	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	move $a1, $t0    	# write the specified characters as seen on assignment PDF
	li   $a2, 511    	# hardcoded buffer length
	syscall

	li   $v0, 16       	# close the file (make sure to check for errors)
	move $a0, $s1
	syscall
	jr $ra			# go back to main routine

#slightly different from Q1.
#use as many arguments as you would like to get this to work.
#make sure the header matches the new dimensions!
