.data

input:	.asciiz "test1.txt"
output:	.asciiz "borded.pgm"	#used as output

borderwidth: .word 1    #specifies border width
buffer:  .space 2048		# buffer for upto 2048 bytes
newbuff: .space 2048
headerbuff: .space 2048  #stores header

#any extra data you specify MUST be after this line 
array: 	.space 672
error1:	.asciiz "File not valid"
text1:  .asciiz "P2\n"
text2:  .asciiz "\n15"
top: 	.space 2048

	.text
	.globl main

main:	la $a0,input		#readfile takes $a0 as input
	jal readfile


	la $a0,buffer		#$a1 will specify the "2D array" we will be flipping
	la $a1,newbuff		#$a2 will specify the buffer that will hold the flipped array.
	la $a2,borderwidth
	jal bord


	la $a0, output		#writefile will take $a0 as file location
	la $a1,newbuff		#$a1 takes location of what we wish to write.
	jal writefile

	li $v0,10		# exit
	syscall

readfile:
	li $v0,13		# Open the file to be read,using $a0
	li $a1, 0		# Open in read-only
	li $a2, 0		# mode is ignored
	syscall
	
	move $s0, $v0        	# use correct file descriptor, and point to buffer	
	blt $s0,0,error_1 	# Conduct error check, to see if file exists

	li $v0, 14           	# read from file
	move $a0, $s0		# load file descriptor
	la $a1, buffer        	# load saved bytes
	li $a2, 2048         	# hardcode maximum number of chars to read
	syscall 
	
	# the text of the string is now in buffer
	li   $v0, 16       	# close the file (make sure to check for errors)
  	move $a0, $s0
  	syscall
  	
  	la $v1, buffer 		# address of the ascii string you just read is returned in $v1.
  	
	jr $ra			# go back to main routine

error_1:
	li $v0,4		# Print error message
	la $a0, error1
	syscall
	li $v0,10		# exit
	syscall



bord: # create array same as previous
	li $t0, -4
	la $t2, ($a0)
	j iterate

iterate:
	
	lb $t3, ($t2)
	beq $t3, 0, print
	bge $t3, 48, number
	add $t2, $t2, 1
	j iterate

number:
	add $t3, $t3, -48
	add $t2, $t2, 1
	lb $t4, ($t2)
	bge $t4, 48, double
	addiu $t0, $t0, 4
	sb $t3, array($t0)
	add $t2, $t2, 1
	j iterate
	double:
	add $t2, $t2, 1
	add $t4, $t4, -48
	mul $t5, $t3, 10
	add $t3, $t5, $t4
	addiu $t0, $t0, 4
	sb $t3, array($t0)
	add $t2, $t2, 1
	j iterate

print: 	la $t6, ($a1) #once it is created...
	li, $a0, 10
	sb $a0, ($t6)
	li $t0, 8
	li $t1, 24
	la $a2, borderwidth
	li $t2, 0
	lb $s3, ($a2)
	li $t3, 0 #We iterate throught the list similarly to cropper, except all elements are included

yax:
	beq $t1, 24, ad
	lb $a0, array($t2)
	blt $a0, 10, secsp
	add $a0, $a0, -10
	add $a0, $a0, 48
	li $a1, 49
	sb $a1, ($t6)
	add $t6, $t6, 1
	sb $a0, ($t6)
	add $t6, $t6, 1
	addiu $t2, $t2, 4
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	secsp:
	add $a0, $a0, 48
	sb $a0, ($t6)
	add $t6, $t6, 1
	addiu $t2, $t2, 4
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, 1
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	ad:
	add $t0, $t0, -1 #Once new line is reached, we increase array position by 1 (next element)
	beq $t0, 0, ex
	li $t7, 0
	add $t1, $0, $0
	beq $t0, 7, done0
		side0:
		beq $t7, $s3, done0 #Before adding newline, we add a series of zeroes followed by spaces. the length of this series is determined by input
		li, $a0, 48
		sb $a0, ($t6)
		add $t6, $t6, 1
		li, $a0, 32
		sb $a0, ($t6)
		add $t6, $t6, 1
		li, $a0, 32
		sb $a0, ($t6)
		add $t6, $t6, 1
		add $t7, $t7, 1
		j side0
	done0:
	li, $a0, 10
	sb $a0, ($t6)
	add $t6, $t6, 1
	li, $t7,0
	side1: #We add this same series of zeroes after the space is added 
		beq $t7, $s3, done
		li, $a0, 48
		sb $a0, ($t6)
		add $t6, $t6, 1
		li, $a0, 32
		sb $a0, ($t6)
		add $t6, $t6, 1
		li, $a0, 32
		sb $a0, ($t6)
		add $t6, $t6, 1
		add $t7, $t7, 1
		j side1
	done:
	beq $t1, 24, ad
	lb $a0, array($t2)
	blt $a0, 10, secsp
	add $a0, $a0, -10
	add $a0, $a0, 48
	li $a1, 49
	sb $a1, ($t6)
	add $t6, $t6, 1
	sb $a0, ($t6)
	add $t6, $t6, 1
	addiu $t2, $t2, 4
	li, $a0, 32
	sb $a0, ($t6)
	add $t6, $t6, 1
	add $t1, $t1, 1
	j yax
	ex:
	li $t7, 0
		side2:
		beq $t7, $s3, done2
		li, $a0, 48
		sb $a0, ($t6)
		add $t6, $t6, 1
		li, $a0, 32
		sb $a0, ($t6)
		add $t6, $t6, 1
		li, $a0, 32
		sb $a0, ($t6)
		add $t6, $t6, 1
		add $t7, $t7, 1
		j side2 
	done2: #We fill the bottom corner with zeroes
	jr $ra
#At this point our buffer has a border on its left and right, bot not top and bottom
#a0=buffer
#a1=newbuff
#a2=borderwidth
#Can assume 24 by 7 as input
#Try to understand the math before coding!
#EXAMPLE: if borderwidth=2, 24 by 7 becomes 28 by 11.

writefile:
	move $t6, $a1
	li   $v0, 13       	# open file to be written to, using $a0.
	li   $a1, 1        	# Open for writing
	li   $a2, 0        	# mode is ignored
	syscall            	# open a file 
	move $s1, $v0      	# save the file descriptor 
	
	lw $t0, borderwidth #load input
	li $t1, 24  #load a buffer used to create the top and bottom borders
	la $s2, top
	li $t3, 0
	add $t1, $t0, $t1
	add $t1, $t1, $t0 #add border width to twice to new file width (once for each side) 
	add $s6 $t1 $0 #save this value
	
		li $a0, 10
		sb $a0, ($s2)
		add $s2, $s2, 1 #add newline to beginning of border buffer
		row:
		beq $t3, $t1, don #Add a zero and two spaces once for every unit in width
		li, $a0, 48
		sb $a0, ($s2)
		add $s2, $s2, 1
		li, $a0, 32
		sb $a0, ($s2)
		add $s2, $s2, 1
		li, $a0, 32
		sb $a0, ($s2)
		add $s2, $s2, 1
		add $t3, $t3, 1
		j row
	don:
	li $t2, 7
	add $t2, $t0, $t2
	add $t2, $t2, $t0 #Calculate new height same way as width
	la $s0 headerbuff #load a buffer used to modify the header
	mul $t7 $t1 7 #Here, both height and width could have 2 digit values
	mul $t7, $t7, 3
	add $t7, $t7, 7
	li $t4 10
	div $t1, $t4 #We start by dividing width by 10
	mflo $s5
	addi $s4, $s5, 48
	sb $s4 ($s0) #store the quotient as the first value
	addi $s0 $s0 1
	mfhi $s5
	addi $t1 $s5 48
	sb $t1 ($s0)  #Store the remainder as second value
	add $s0, $s0, 1
	addi $t1 $0 32
	sb $t1 ($s0) #add space
	add $s0, $s0, 1 #repeat for height
	div $t2, $t4
	mflo $s5
	addi $s4, $s5, 48
	sb $s4 ($s0) 
	addi $s0 $s0 1
	mfhi $s5
	addi $t2 $s5 48
	sb $t2 ($s0) 
	add $s0, $s0, 1

	
	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	la   $a1, text1    	# write the specified characters as seen on assignment PDF
	li   $a2, 3     	# hardcoded buffer length
	syscall
	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	la   $a1, headerbuff    	# write the specified characters as seen on assignment PDF
	li   $a2, 5     	# hardcoded buffer length
	syscall
	
	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	la   $a1, text2    	# write the specified characters as seen on assignment PDF
	li   $a2, 3     	# hardcoded buffer length
	syscall
	mul $s6 $s6 3
	li $t3 0

	seti:
	beq $t3, $t0, d
	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	la $a1, top   	# write the specified characters as seen on assignment PDF
	move $a2, $s6     	# hardcoded buffer length
	add $t3 $t3 1
	syscall
	j seti
	d:
	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	move $a1, $t6    	# write the specified characters as seen on assignment PDF
	add $a2, $0, $t7     	# hardcoded buffer length
	syscall
	
	li $t3 0

	set:
	beq $t3, $t0, do
	li   $v0, 15       	# system call for write to file
	move $a0, $s1     	# file descriptor 
	la $a1, top   	# write the specified characters as seen on assignment PDF
	move $a2, $s6     	# hardcoded buffer length
	add $t3 $t3 1
	syscall
	j set
	do:
	
	li   $v0, 16       	# close the file (make sure to check for errors)
	move $a0, $s1
	syscall
	jr $ra			
jr $ra
#slightly different from Q1.
#use as many arguments as you would like to get this to work.
#make sure the header matches the new dimensions!
